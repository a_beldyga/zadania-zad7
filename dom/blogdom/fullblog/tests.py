from django.test import TestCase
from django.test.client import Client
import unittest
from django.contrib.auth.models import User


# Create your tests here.

class TestClass(TestCase):

    def testSetup(self):
        User.objects.create_user(username='p6', password='p6')
        self.client.login(username='p6', password='p6')

    def test_user_login(self):
        response = self.client.post('/login/', {'username': 'p6', 'password': 'p6'}, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_get_index(self):
        response = self.client.get('/blog/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('newsy' in response.context)

    def test_post_new_post(self):
        self.client.post('/login/', {'username': 'p6', 'password': 'p6'}, follow=True)
        response = self.client.post('/blog/add/', {'title': 'Title', 'content': 'Content'}, follow=True)
        self.assertEqual(response.status_code, 200)


    def test_group_posts_by_user(self):
        response = self.client.get('/blog/user/p6')
        self.assertTrue('p6' in response.content)
        self.assertTrue(not('user1' in response.content))




	



