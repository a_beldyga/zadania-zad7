from django import forms
from django.contrib.auth.models import User

class Form(forms.Form):
   # user = forms.CharField(max_length=100)
    title = forms.CharField(max_length=1000)
    content=forms.CharField(max_length=100000)


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'password')


