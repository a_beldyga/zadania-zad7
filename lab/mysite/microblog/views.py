from models import Table, Form
from datetime import date
from django.shortcuts import render_to_response, redirect
from django.core.context_processors import csrf
from django.core.urlresolvers import reverse


# Create your views here.

def blog(request):
    wpisy = Table.objects.all()
    return render_to_response('base.html', {'wpisy': wpisy})



def add(request):
    if request.method == "GET":
        c = {}
        c.update(csrf(request))
        return render_to_response('add.html', c)
    if request.method == 'POST':
        form = Form(request.POST)
        if form.is_valid():
            data = Form.cleaned_data
            user = data['user']
            title = data['title']
            content = data['content']
            published = date.today()
            r = Table(user=user, title=title, content=content, published=published)
            r.save()

            return redirect(reverse('glowna'))
        else:
            c = {}
            c.update(csrf(request))
            return render_to_response('add.html', c)
