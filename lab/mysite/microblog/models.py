from django.db import models
from django import forms

# Create your models here.
class Table(models.Model):
    user = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    content = models.TextField()
    published = models.DateField()

    def __unicode__(self):
        return self.user

class Form(forms.Form):
    user = forms.CharField(max_length=255)
    title = forms.CharField(max_length=255)
    content = forms.TextField()
    published = forms.DateField()